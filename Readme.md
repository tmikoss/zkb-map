# Usage

* Include jQuery, Three.js libraries
* Include `/public/js/zkb-map.min.js`
* Make files in `/public/data/` and `/public/images` available
* Run `$('#dom-element-that-will-contain-map').ZKBMap()`

Call `calibrateCamera` function on object returned from `ZKBMap` when container size changes.

Refer to `/public/index.html` for more detailed use.

Nearly everything can be configured. Refer to `$.fn.ZKBMap` definition in `/source/zkb-map.coffee` for options and their default values.

# Dev dependencies

* Have Ruby (1.9.3 or up) installed
* Run `gem install bundler` to install Bundler dependency manager
* Run `bundle install` to install dependencies

Run `guard` executable at repository root - this will watch source files and re-generate them as needed.

# Updating static data

Install dev dependencies and run `rake static_data:update` to re-generate static data.

Needs EvE mysql database dump available locally. Read `/Rakefile` if your configuration differs, or you just want the queries.
