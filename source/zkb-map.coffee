killTemplate = Handlebars.compile """
  <div class='zkb-map-kill' id='zkb-map-kill-{{killID}}' data-solar-system='{{solarSystemID}}' >
    <a href='//zkillboard.com/detail/{{killID}}/' target='_blank'>
      <img src='https://image.eveonline.com/Type/{{shipTypeID}}_64.png'>
    </a>
    <a href='//zkillboard.com/character/{{characterID}}/' target='_blank'>
      <img src='https://image.eveonline.com/Character/{{characterID}}_64.jpg'>
    </a>
    <a href='//zkillboard.com/corporation/{{corporationID}}/' target='_blank'>
      <img src='https://image.eveonline.com/Corporation/{{corporationID}}_64.png'>
    </a>
    {{#if allianceID}}
      <a href='//zkillboard.com/alliance/{{allianceID}}/' target='_blank'>
        <img src='https://image.eveonline.com/Alliance/{{allianceID}}_64.png'>
      </a>
    {{/if}}
  </div>
"""

regionSelectorTemplate = Handlebars.compile """
  <div class='zkb-map-region-selector'>
    <ul>
      {{#each groups}}
        <li data-name='{{this.name}}'>{{this.name}}</li>
      {{/each}}
    </ul>
  </div>
"""

infoPopupTemplate = Handlebars.compile """
  <div class='zkb-map-info-popup'>
    <div class='zkb-map-info-popup-text'>
      {{name}}
      <br>
      {{value}}
    </div>
    <img src='{{src}}'></img>
  </div>
"""

asIsk = (value) ->
  return unless value

  str = value.toString()
  str = str.split('').reverse().join('')
  str = str.match(/.{1,3}/g).join(' ')
  str = str.split('').reverse().join('')
  str + ' ISK'

class ZKBMap
  allSolarSystems: {}
  displayedSolarSystems: {}
  regionGroups: []
  solarSystemPings: {}
  killsReceivedFilter: {}

  constructor: (@options) ->
    @initializeCamera()
    @initializeDOM()
    @initializeColors()
    @fetchData()
    @initializeRotationControls()
    @listenToKillFeed()
    @render()
    setInterval TWEEN.update, 15

  initializeDOM: ->
    @options.container.addClass 'zkb-map-container'

    @domCanvas  = $ @renderer.domElement
    @domCanvas.addClass 'zkb-map-canvas'
    @options.container.append @domCanvas

    @domKillLog = $ "<div class='zkb-map-kill-log'></div>"
    @options.container.append @domKillLog

  initializeCamera: ->
    @galaxyScene = new THREE.Scene()
    @pingScene   = new THREE.Scene()

    @renderer = new THREE.WebGLRenderer
      antialias: true
    @renderer.autoClear = false

    @projector = new THREE.Projector()

    @camera = new THREE.PerspectiveCamera @options.cameraFov

    @calibrateCamera()

  initializeColors: ->
    @securityGradient = new Rainbow()
    @securityGradient.setNumberRange -1 * @options.colorSteps, @options.colorSteps
    @securityGradient.setSpectrum @options.lowSecColor, @options.highSecColor

    @solarSystemMaterial = new THREE.ParticleBasicMaterial
      vertexColors: true
      size: 6
      map: THREE.ImageUtils.loadTexture @options.starTextureUrl
      blending: THREE.AdditiveBlending
      transparent: true

    @glowMaterial = new THREE.SpriteMaterial
      map: THREE.ImageUtils.loadTexture @options.starTextureUrl
      useScreenCoordinates: false
      color: new THREE.Color @options.killFlareColor
      blending: THREE.AdditiveBlending

  securityToColor: (security) ->
    new THREE.Color "##{@securityGradient.colourAt Math.round(security * @options.colorSteps)}"

  emptyScenes: ->
    for systemId, ping of @solarSystemPings
      ping.animation?.stop()
      @pingScene.remove ping.glow

    for object in @galaxyScene.children
      @galaxyScene.remove object

    @domKillLog.html ''

  fetchData: ->
    solarSystemCallback = (items) =>
      for item in items
        solarSystem =
          id:       item[0]
          position: new THREE.Vector3 item[1], item[3], item[2]
          security: item[4]
          region:   item[5]
          name:     item[6]
        @allSolarSystems[solarSystem.id] = solarSystem

    regionGroupsCallback = (items) =>
      for item in items
        regionGroup =
          name:        item[0]
          center:      new THREE.Vector3 item[1], item[3], item[2]
          regions:     item[4]
          minSecurity: item[5]
          maxSecurity: item[6]
        @regionGroups.push regionGroup

    $.when(
      $.getJSON(@options.solarSystemsJsonUrl, solarSystemCallback),
      $.getJSON(@options.regionGroupsJsonUrl, regionGroupsCallback)
    ).then =>
      @initializeRegionSelector() if @options.showRegionSwitcher
      @displayRegionGroup @regionGroups[0].name

  initializeRegionSelector: ->
    @domRegionSelector = $ regionSelectorTemplate { groups: @regionGroups }
    @options.container.prepend @domRegionSelector
    @domRegionSelector.on 'click', 'li', (e) =>
      @displayRegionGroup $.trim $(e.target).html()

  displayRegionGroup: (regionGroupName) ->
    regionGroup = null
    for rg in @regionGroups
      if rg.name is regionGroupName
        regionGroup = rg
        break
    return unless regionGroup

    if @options.showRegionSwitcher
      @domRegionSelector.find('li').removeClass 'current'
      @domRegionSelector.find("li[data-name='#{regionGroup.name}']").addClass 'current'

    @displayedSolarSystems = {}
    geometry  = new THREE.Geometry()
    maxOffset = 0

    for solarSystemID, solarSystem of @allSolarSystems
      if solarSystem.region in regionGroup.regions and regionGroup.minSecurity <= solarSystem.security <= regionGroup.maxSecurity
        # We need just the position in 'working set' of data, to know where to draw pings
        clonedSolarSystem = { position: solarSystem.position.clone() }
        # Offset the center of every star by the regional average - this way camera can be kept pointing at 0,0,0 at all times
        clonedSolarSystem.position.sub(regionGroup.center)
        @displayedSolarSystems[solarSystemID] = clonedSolarSystem

        geometry.vertices.push clonedSolarSystem.position
        geometry.colors.push @securityToColor solarSystem.security

        maxOffset = Math.max(maxOffset, Math.abs(clonedSolarSystem.position.x), Math.abs(clonedSolarSystem.position.y), Math.abs(clonedSolarSystem.position.z))

    galaxy = new THREE.ParticleSystem geometry, @solarSystemMaterial
    galaxy.sortParticles = true

    @emptyScenes()
    @galaxyScene.add galaxy

    @camera.position = new THREE.Vector3 0, 0, maxOffset + 100
    @camera.lookAt new THREE.Vector3 0, 0, 0

  initializeRotationControls: ->
    @controls = new THREE.OrbitControls @camera, @domCanvas[0]

  ping: (kill) ->
    systemId    = kill.solarSystemID

    solarSystem = @displayedSolarSystems[systemId]

    return unless solarSystem

    currentPing = @solarSystemPings[systemId] || {}

    unless currentPing.glow
      currentPing.glow = new THREE.Sprite @glowMaterial
      currentPing.glow.position = solarSystem.position
      @pingScene.add currentPing.glow

    currentPing.animation?.stop()

    maxFlare = new THREE.Vector3 @options.killFlareSize, @options.killFlareSize
    minFlare = new THREE.Vector3 0, 0

    flare = new TWEEN.Tween(currentPing.glow.scale).to(maxFlare, @options.killFlareDuration).easing(TWEEN.Easing.Exponential.In)
    fade  = new TWEEN.Tween(currentPing.glow.scale).to(minFlare, @options.killFadeDuration).easing(TWEEN.Easing.Exponential.Out)

    flare.onComplete =>
      fade.start()
      currentPing.animation = fade
    fade.onComplete =>
      @pingScene.remove currentPing.glow
      delete @solarSystemPings[systemId]

    currentPing.animation = flare
    flare.start()
    @solarSystemPings[systemId] = currentPing

    @addToKillLog kill

  addToKillLog: (kill) ->
    delete kill.allianceID if kill.allianceID is '0'

    killDom = $ killTemplate kill

    infoPopupDom = $ infoPopupTemplate
      src: @options.infoPopupTextureUrl
      name: @allSolarSystems[kill.solarSystemID]?.name
      involved: kill.involved
      value: asIsk kill.totalValue

    @domKillLog.prepend killDom

    killDom.fadeIn @options.killFlareDuration, =>
      killDom.fadeOut @options.killFadeDuration * 1.5, =>
        killDom.remove()
        infoPopupDom.remove()
        delete @killsReceivedFilter[kill.killID]

    killDom.mouseover =>
      infoPopupDom.css @infoPopupLocation kill.solarSystemID
      @options.container.append infoPopupDom

    killDom.mouseout ->
      infoPopupDom.detach()

  render: =>
    @controls.update()
    @renderer.clear()
    @renderer.render @galaxyScene, @camera
    @renderer.clear false, true, false
    @renderer.render @pingScene, @camera
    requestAnimationFrame @render

  calibrateCamera: =>
    @camera.aspect = @options.container.innerWidth() / @options.container.innerHeight()
    @camera.updateProjectionMatrix()
    @renderer.setSize @options.container.innerWidth(), @options.container.innerHeight()

  listenToKillFeed: ->
    try
      @client = new WebSocket @options.killFeedWebsocket
      @client.onmessage = (event) =>
        kill = JSON.parse event.data

        # the same kill can be received by zkb via many channels (ex: old api + crest), and every instance appears in the feed.
        # this lets through only the first report of the kill
        return if @killsReceivedFilter[kill.killID]
        @killsReceivedFilter[kill.killID] = true

        @ping kill
    catch e
      console.error e

  infoPopupLocation: (systemId) ->
    position  = @displayedSolarSystems[systemId].position.clone()
    projected = @projector.projectVector position, @camera

    scaleX = (projected.x + 1) / 2
    scaleY = (-projected.y + 1) / 2

    css =
      top: @options.container.innerHeight() * scaleY
      left: @options.container.innerWidth() * scaleX

$.fn.ZKBMap = (options) ->
  defaults =
    container: this
    cameraFov: 90
    solarSystemsJsonUrl: '/data/solar_systems.json?1400612823'
    regionGroupsJsonUrl: '/data/region_groups.json'
    starTextureUrl: '/images/glow.png'
    colorSteps: 20
    lowSecColor: '#999999'
    highSecColor: '#CCCCCC'
    killFeedWebsocket: 'wss://ws.eve-kill.net/map/'
    killFlareSize: 100
    killFlareDuration: 500
    killFadeDuration: 15000
    killFlareColor: '#FF0000'
    showRegionSwitcher: true
    infoPopupTextureUrl: '/images/pointer.png'

  new ZKBMap($.extend defaults, options)
