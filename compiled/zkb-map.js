(function() {
  var ZKBMap, asIsk, infoPopupTemplate, killTemplate, regionSelectorTemplate,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  killTemplate = Handlebars.compile("<div class='zkb-map-kill' id='zkb-map-kill-{{killID}}' data-solar-system='{{solarSystemID}}' >\n  <a href='//zkillboard.com/detail/{{killID}}/' target='_blank'>\n    <img src='https://image.eveonline.com/Type/{{shipTypeID}}_64.png'>\n  </a>\n  <a href='//zkillboard.com/character/{{characterID}}/' target='_blank'>\n    <img src='https://image.eveonline.com/Character/{{characterID}}_64.jpg'>\n  </a>\n  <a href='//zkillboard.com/corporation/{{corporationID}}/' target='_blank'>\n    <img src='https://image.eveonline.com/Corporation/{{corporationID}}_64.png'>\n  </a>\n  {{#if allianceID}}\n    <a href='//zkillboard.com/alliance/{{allianceID}}/' target='_blank'>\n      <img src='https://image.eveonline.com/Alliance/{{allianceID}}_64.png'>\n    </a>\n  {{/if}}\n</div>");

  regionSelectorTemplate = Handlebars.compile("<div class='zkb-map-region-selector'>\n  <ul>\n    {{#each groups}}\n      <li data-name='{{this.name}}'>{{this.name}}</li>\n    {{/each}}\n  </ul>\n</div>");

  infoPopupTemplate = Handlebars.compile("<div class='zkb-map-info-popup'>\n  <div class='zkb-map-info-popup-text'>\n    {{name}}\n    <br>\n    {{value}}\n  </div>\n  <img src='{{src}}'></img>\n</div>");

  asIsk = function(value) {
    var str;
    if (!value) {
      return;
    }
    str = value.toString();
    str = str.split('').reverse().join('');
    str = str.match(/.{1,3}/g).join(' ');
    str = str.split('').reverse().join('');
    return str + ' ISK';
  };

  ZKBMap = (function() {
    ZKBMap.prototype.allSolarSystems = {};

    ZKBMap.prototype.displayedSolarSystems = {};

    ZKBMap.prototype.regionGroups = [];

    ZKBMap.prototype.solarSystemPings = {};

    ZKBMap.prototype.killsReceivedFilter = {};

    function ZKBMap(options) {
      this.options = options;
      this.calibrateCamera = __bind(this.calibrateCamera, this);
      this.render = __bind(this.render, this);
      this.initializeCamera();
      this.initializeDOM();
      this.initializeColors();
      this.fetchData();
      this.initializeRotationControls();
      this.listenToKillFeed();
      this.render();
      setInterval(TWEEN.update, 15);
    }

    ZKBMap.prototype.initializeDOM = function() {
      this.options.container.addClass('zkb-map-container');
      this.domCanvas = $(this.renderer.domElement);
      this.domCanvas.addClass('zkb-map-canvas');
      this.options.container.append(this.domCanvas);
      this.domKillLog = $("<div class='zkb-map-kill-log'></div>");
      return this.options.container.append(this.domKillLog);
    };

    ZKBMap.prototype.initializeCamera = function() {
      this.galaxyScene = new THREE.Scene();
      this.pingScene = new THREE.Scene();
      this.renderer = new THREE.WebGLRenderer({
        antialias: true
      });
      this.renderer.autoClear = false;
      this.projector = new THREE.Projector();
      this.camera = new THREE.PerspectiveCamera(this.options.cameraFov);
      return this.calibrateCamera();
    };

    ZKBMap.prototype.initializeColors = function() {
      this.securityGradient = new Rainbow();
      this.securityGradient.setNumberRange(-1 * this.options.colorSteps, this.options.colorSteps);
      this.securityGradient.setSpectrum(this.options.lowSecColor, this.options.highSecColor);
      this.solarSystemMaterial = new THREE.ParticleBasicMaterial({
        vertexColors: true,
        size: 6,
        map: THREE.ImageUtils.loadTexture(this.options.starTextureUrl),
        blending: THREE.AdditiveBlending,
        transparent: true
      });
      return this.glowMaterial = new THREE.SpriteMaterial({
        map: THREE.ImageUtils.loadTexture(this.options.starTextureUrl),
        useScreenCoordinates: false,
        color: new THREE.Color(this.options.killFlareColor),
        blending: THREE.AdditiveBlending
      });
    };

    ZKBMap.prototype.securityToColor = function(security) {
      return new THREE.Color("#" + (this.securityGradient.colourAt(Math.round(security * this.options.colorSteps))));
    };

    ZKBMap.prototype.emptyScenes = function() {
      var object, ping, systemId, _i, _len, _ref, _ref1, _ref2;
      _ref = this.solarSystemPings;
      for (systemId in _ref) {
        ping = _ref[systemId];
        if ((_ref1 = ping.animation) != null) {
          _ref1.stop();
        }
        this.pingScene.remove(ping.glow);
      }
      _ref2 = this.galaxyScene.children;
      for (_i = 0, _len = _ref2.length; _i < _len; _i++) {
        object = _ref2[_i];
        this.galaxyScene.remove(object);
      }
      return this.domKillLog.html('');
    };

    ZKBMap.prototype.fetchData = function() {
      var regionGroupsCallback, solarSystemCallback,
        _this = this;
      solarSystemCallback = function(items) {
        var item, solarSystem, _i, _len, _results;
        _results = [];
        for (_i = 0, _len = items.length; _i < _len; _i++) {
          item = items[_i];
          solarSystem = {
            id: item[0],
            position: new THREE.Vector3(item[1], item[3], item[2]),
            security: item[4],
            region: item[5],
            name: item[6]
          };
          _results.push(_this.allSolarSystems[solarSystem.id] = solarSystem);
        }
        return _results;
      };
      regionGroupsCallback = function(items) {
        var item, regionGroup, _i, _len, _results;
        _results = [];
        for (_i = 0, _len = items.length; _i < _len; _i++) {
          item = items[_i];
          regionGroup = {
            name: item[0],
            center: new THREE.Vector3(item[1], item[3], item[2]),
            regions: item[4],
            minSecurity: item[5],
            maxSecurity: item[6]
          };
          _results.push(_this.regionGroups.push(regionGroup));
        }
        return _results;
      };
      return $.when($.getJSON(this.options.solarSystemsJsonUrl, solarSystemCallback), $.getJSON(this.options.regionGroupsJsonUrl, regionGroupsCallback)).then(function() {
        if (_this.options.showRegionSwitcher) {
          _this.initializeRegionSelector();
        }
        return _this.displayRegionGroup(_this.regionGroups[0].name);
      });
    };

    ZKBMap.prototype.initializeRegionSelector = function() {
      var _this = this;
      this.domRegionSelector = $(regionSelectorTemplate({
        groups: this.regionGroups
      }));
      this.options.container.prepend(this.domRegionSelector);
      return this.domRegionSelector.on('click', 'li', function(e) {
        return _this.displayRegionGroup($.trim($(e.target).html()));
      });
    };

    ZKBMap.prototype.displayRegionGroup = function(regionGroupName) {
      var clonedSolarSystem, galaxy, geometry, maxOffset, regionGroup, rg, solarSystem, solarSystemID, _i, _len, _ref, _ref1, _ref2, _ref3;
      regionGroup = null;
      _ref = this.regionGroups;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        rg = _ref[_i];
        if (rg.name === regionGroupName) {
          regionGroup = rg;
          break;
        }
      }
      if (!regionGroup) {
        return;
      }
      if (this.options.showRegionSwitcher) {
        this.domRegionSelector.find('li').removeClass('current');
        this.domRegionSelector.find("li[data-name='" + regionGroup.name + "']").addClass('current');
      }
      this.displayedSolarSystems = {};
      geometry = new THREE.Geometry();
      maxOffset = 0;
      _ref1 = this.allSolarSystems;
      for (solarSystemID in _ref1) {
        solarSystem = _ref1[solarSystemID];
        if ((_ref2 = solarSystem.region, __indexOf.call(regionGroup.regions, _ref2) >= 0) && (regionGroup.minSecurity <= (_ref3 = solarSystem.security) && _ref3 <= regionGroup.maxSecurity)) {
          clonedSolarSystem = {
            position: solarSystem.position.clone()
          };
          clonedSolarSystem.position.sub(regionGroup.center);
          this.displayedSolarSystems[solarSystemID] = clonedSolarSystem;
          geometry.vertices.push(clonedSolarSystem.position);
          geometry.colors.push(this.securityToColor(solarSystem.security));
          maxOffset = Math.max(maxOffset, Math.abs(clonedSolarSystem.position.x), Math.abs(clonedSolarSystem.position.y), Math.abs(clonedSolarSystem.position.z));
        }
      }
      galaxy = new THREE.ParticleSystem(geometry, this.solarSystemMaterial);
      galaxy.sortParticles = true;
      this.emptyScenes();
      this.galaxyScene.add(galaxy);
      this.camera.position = new THREE.Vector3(0, 0, maxOffset + 100);
      return this.camera.lookAt(new THREE.Vector3(0, 0, 0));
    };

    ZKBMap.prototype.initializeRotationControls = function() {
      return this.controls = new THREE.OrbitControls(this.camera, this.domCanvas[0]);
    };

    ZKBMap.prototype.ping = function(kill) {
      var currentPing, fade, flare, maxFlare, minFlare, solarSystem, systemId, _ref,
        _this = this;
      systemId = kill.solarSystemID;
      solarSystem = this.displayedSolarSystems[systemId];
      if (!solarSystem) {
        return;
      }
      currentPing = this.solarSystemPings[systemId] || {};
      if (!currentPing.glow) {
        currentPing.glow = new THREE.Sprite(this.glowMaterial);
        currentPing.glow.position = solarSystem.position;
        this.pingScene.add(currentPing.glow);
      }
      if ((_ref = currentPing.animation) != null) {
        _ref.stop();
      }
      maxFlare = new THREE.Vector3(this.options.killFlareSize, this.options.killFlareSize);
      minFlare = new THREE.Vector3(0, 0);
      flare = new TWEEN.Tween(currentPing.glow.scale).to(maxFlare, this.options.killFlareDuration).easing(TWEEN.Easing.Exponential.In);
      fade = new TWEEN.Tween(currentPing.glow.scale).to(minFlare, this.options.killFadeDuration).easing(TWEEN.Easing.Exponential.Out);
      flare.onComplete(function() {
        fade.start();
        return currentPing.animation = fade;
      });
      fade.onComplete(function() {
        _this.pingScene.remove(currentPing.glow);
        return delete _this.solarSystemPings[systemId];
      });
      currentPing.animation = flare;
      flare.start();
      this.solarSystemPings[systemId] = currentPing;
      return this.addToKillLog(kill);
    };

    ZKBMap.prototype.addToKillLog = function(kill) {
      var infoPopupDom, killDom, _ref,
        _this = this;
      if (kill.allianceID === '0') {
        delete kill.allianceID;
      }
      killDom = $(killTemplate(kill));
      infoPopupDom = $(infoPopupTemplate({
        src: this.options.infoPopupTextureUrl,
        name: (_ref = this.allSolarSystems[kill.solarSystemID]) != null ? _ref.name : void 0,
        involved: kill.involved,
        value: asIsk(kill.totalValue)
      }));
      this.domKillLog.prepend(killDom);
      killDom.fadeIn(this.options.killFlareDuration, function() {
        return killDom.fadeOut(_this.options.killFadeDuration * 1.5, function() {
          killDom.remove();
          infoPopupDom.remove();
          return delete _this.killsReceivedFilter[kill.killID];
        });
      });
      killDom.mouseover(function() {
        infoPopupDom.css(_this.infoPopupLocation(kill.solarSystemID));
        return _this.options.container.append(infoPopupDom);
      });
      return killDom.mouseout(function() {
        return infoPopupDom.detach();
      });
    };

    ZKBMap.prototype.render = function() {
      this.controls.update();
      this.renderer.clear();
      this.renderer.render(this.galaxyScene, this.camera);
      this.renderer.clear(false, true, false);
      this.renderer.render(this.pingScene, this.camera);
      return requestAnimationFrame(this.render);
    };

    ZKBMap.prototype.calibrateCamera = function() {
      this.camera.aspect = this.options.container.innerWidth() / this.options.container.innerHeight();
      this.camera.updateProjectionMatrix();
      return this.renderer.setSize(this.options.container.innerWidth(), this.options.container.innerHeight());
    };

    ZKBMap.prototype.listenToKillFeed = function() {
      var e,
        _this = this;
      try {
        this.client = new WebSocket(this.options.killFeedWebsocket);
        return this.client.onmessage = function(event) {
          var kill;
          kill = JSON.parse(event.data);
          if (_this.killsReceivedFilter[kill.killID]) {
            return;
          }
          _this.killsReceivedFilter[kill.killID] = true;
          return _this.ping(kill);
        };
      } catch (_error) {
        e = _error;
        return console.error(e);
      }
    };

    ZKBMap.prototype.infoPopupLocation = function(systemId) {
      var css, position, projected, scaleX, scaleY;
      position = this.displayedSolarSystems[systemId].position.clone();
      projected = this.projector.projectVector(position, this.camera);
      scaleX = (projected.x + 1) / 2;
      scaleY = (-projected.y + 1) / 2;
      return css = {
        top: this.options.container.innerHeight() * scaleY,
        left: this.options.container.innerWidth() * scaleX
      };
    };

    return ZKBMap;

  })();

  $.fn.ZKBMap = function(options) {
    var defaults;
    defaults = {
      container: this,
      cameraFov: 90,
      solarSystemsJsonUrl: '/data/solar_systems.json?1400612823',
      regionGroupsJsonUrl: '/data/region_groups.json',
      starTextureUrl: '/images/glow.png',
      colorSteps: 20,
      lowSecColor: '#999999',
      highSecColor: '#CCCCCC',
      killFeedWebsocket: 'wss://ws.eve-kill.net/map/',
      killFlareSize: 100,
      killFlareDuration: 500,
      killFadeDuration: 15000,
      killFlareColor: '#FF0000',
      showRegionSwitcher: true,
      infoPopupTextureUrl: '/images/pointer.png'
    };
    return new ZKBMap($.extend(defaults, options));
  };

}).call(this);
