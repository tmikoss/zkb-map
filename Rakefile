namespace :static_data do
  desc "Update static data"
  task :update do
    require 'mysql2'
    require 'multi_json'

    @scale  = 1000000000000000
    @client = Mysql2::Client.new(host: 'localhost', username: 'root', database: 'eve')

    def dump_json(file, data)
      File.open(File.join(File.dirname(__FILE__), 'public', 'data', file), 'w') do |file|
        file.write MultiJson.dump(data, pretty: false)
      end
    end

    def avg(array, field)
      values = array.map{ |r| r[field] }
      values.reduce(:+) / values.size
    end

    def max(array, field)
      array.map{ |r| r[field] }.max
    end

    def min(array, field)
      array.map{ |r| r[field] }.min
    end

    def region_group(name, where_condition)
      # You can't trust x/y/z properties of region itself - these tend to be different from the actual averages in some cases (mainly WHs)
      regions = @client.query <<-SQL
        SELECT r.regionID,
               ROUND(avg(s.x) / #{@scale}, 4) as x,
               ROUND(avg(s.y) / #{@scale}, 4) as y,
               ROUND(avg(s.z) / #{@scale}, 4) as z,
               ROUND(min(s.security), 4) as minsecurity,
               ROUND(max(s.security), 4) as maxsecurity
          FROM mapRegions r, mapSolarSystems s
         WHERE #{where_condition}
           AND r.regionID = s.regionID
         GROUP BY r.regionID
         ORDER BY r.regionID
      SQL

      [name, avg(regions, 'x'), avg(regions, 'y'), avg(regions, 'z'), regions.map{|r| r['regionID'] }, min(regions, 'minsecurity'), max(regions, 'maxsecurity') ]
    end


    solar_systems_query = <<-SQL
      SELECT s.solarSystemID,
             ROUND(s.x / #{@scale}, 4) as x,
             ROUND(s.y / #{@scale}, 4) as y,
             ROUND(s.z / #{@scale}, 4) as z,
             ROUND(s.security, 4) as security,
             s.regionID,
             CONCAT(s.solarSystemName, ' / ', r.regionName) name
        FROM mapSolarSystems s, mapRegions r
       WHERE r.regionID = s.regionID
       ORDER BY solarSystemID
    SQL

    dump_json 'solar_systems.json', @client.query(solar_systems_query).map(&:values)

    region_groups = []

    region_groups << region_group("New Eden", "r.regionID < 11000000")

    region_groups << region_group("Wormholes", "r.regionID >= 11000000")

    region_groups << region_group("Hisec", "r.regionID < 11000000 AND s.security > 0.4")

    region_groups << region_group("Lowsec", "r.regionID < 11000000 AND s.security <= 0.4 AND s.security > 0")

    region_groups << region_group("Null", "r.regionID < 11000000 AND s.security <= 0")

    dump_json 'region_groups.json', region_groups
  end
end
